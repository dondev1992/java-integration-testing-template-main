package io.catalyte.training.controllers;

import io.catalyte.training.entities.Pet;
import io.catalyte.training.repositories.PetRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.ArrayList;
import java.util.List;
import static io.catalyte.training.constants.StringConstants.CONTEXT_PETS;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class PetControllerTest {

    ResultMatcher okStatus = MockMvcResultMatchers.status().isOk();
    ResultMatcher createdStatus = MockMvcResultMatchers.status().isCreated();
    ResultMatcher deletedStatus = MockMvcResultMatchers.status().isNoContent();
    ResultMatcher notFoundStatus = MockMvcResultMatchers.status().isNotFound();
    ResultMatcher badRequestStatus = MockMvcResultMatchers.status().isBadRequest();

    ResultMatcher expectedType = MockMvcResultMatchers.content()
            .contentType(MediaType.APPLICATION_JSON);

    @Autowired
    private WebApplicationContext wac;

    @Autowired
    private PetRepository petRepository;
    private MockMvc mockMvc;

    @BeforeEach
    public void setUp() {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();

    }


    @Test
    public void getPetThatDoesExistById() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS + "/1"))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Cletus")));
    }


    @Test
    public void getPetThatDoesNotExist() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS + "/65328"))
                .andExpect(notFoundStatus);
    }

    @Test
    public void queryAllPetsReturnsAtLeastTwo() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", hasSize(greaterThan(2))));
    }

    @Test
    public void saveAllPets() throws Exception {
        List<String> json = new ArrayList<>();
        json.add(
                "{\"name\":\"Hannibal\",\"breed\":\"Dog\",\"age\":2}");
        json.add(
                "{\"name\":\"Adolf\",\"breed\":\"Alligator\",\"age\":4}");
        json.add(
                "{\"name\":\"Hannibal\",\"breed\":\"Dog\",\"age\":2}");
        json.add(
                "{\"name\":\"Adolf\",\"breed\":\"Alligator\",\"age\":4}");
        json.add(
                "{\"name\":\"Hannibal\",\"breed\":\"Dog\",\"age\":2}");
        json.add(
                "{\"name\":\"Adolf\",\"breed\":\"Alligator\",\"age\":4}");
        json.add(
                "{\"name\":\"Hannibal\",\"breed\":\"Dog\",\"age\":2}");
        json.add(
                "{\"name\":\"Adolf\",\"breed\":\"Alligator\",\"age\":4}");

        this.mockMvc
                .perform(post(CONTEXT_PETS + "/all")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(String.valueOf(json)))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$", isA(ArrayList.class)))
                .andExpect(jsonPath("$", hasSize(8)));
    }

    @Test
    public void addNewPet() throws Exception {
        String json = "{\"name\":\"Boo\",\"breed\":\"chicken\",\"age\":1}";
        this.mockMvc
                .perform(post(CONTEXT_PETS)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(createdStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.name", is("Boo")));
    }

    @Test
    public void putPet() throws Exception {
        String json = "{\"id\":1,\"name\":\"Cletus\",\"breed\":\"Dog\",\"age\":8}";
        this.mockMvc
                .perform(put(CONTEXT_PETS + "/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(okStatus)
                .andExpect(expectedType)
                .andExpect(jsonPath("$.age", is(8)));
    }

    @Test
    public void getPetBadRequest() throws Exception {
        mockMvc
                .perform(get(CONTEXT_PETS + "/garbage"))
                .andExpect(badRequestStatus);
    }

    @Test
    public void deletePet() throws Exception {
        Pet pet = new Pet("Bubba", "Rattlesnake", 21);
        petRepository.save(pet);
        mockMvc
                .perform(delete(CONTEXT_PETS + "/4"))
                .andExpect(deletedStatus);
    }

}